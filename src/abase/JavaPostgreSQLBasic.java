/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BAPA
 */
class JavaPostgreSQLBasic {
Connection connection;
    PreparedStatement stmt, stmt2;
     public void connectDatabase() {
        try {
            // We register the PostgreSQL driver
            // Registramos el driver de PostgresSQL
            try { 
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException ex) {
                System.out.println("Error al registrar el driver de PostgreSQL: " + ex);
            }
            connection = null;
            // Database connect
            // Conectamos con la base de datos
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost/byron",
                    "openpg", "openpgpwd");
            boolean valid = connection.isValid(50000);
            if(valid == true){
                System.out.println("Correcto");
                Random r = new Random();
                int principal = r.nextInt(50000);
                int detalle =r.nextInt(50000);
                InsertarPrincipal(principal);
                InsertarDetalle(detalle, principal);
                
                       
                
            }else{
                System.out.println("fallo");
            }
            //System.out.println(valid ? "TEST OK" : "TEST FAIL");
            
        } catch (java.sql.SQLException sqle) {
            System.out.println("Error: " + sqle);
        }
    } 
     
    private void InsertarPrincipal(int id){
        try {
            stmt = connection.prepareStatement("INSERT INTO principal (id, descripcion, idusuario, total) VALUES (?,?,?,?)");
            stmt.setInt(1, id);
            stmt.setString(2, "holaaaa mundo");
            stmt.setInt(3, 40000);
            stmt.setInt(4, 5);
            stmt.execute();
            System.out.println("Insertados principal");
        } catch (SQLException ex) {
            Logger.getLogger(JavaPostgreSQLBasic.class.getName()).log(Level.SEVERE, null, ex);
        }
               
                
    }

    private void InsertarDetalle(int id, int idP){
       
        try {
            stmt2 = connection.prepareStatement("INSERT INTO detalle (id, principal_id, clavepredial, descripcion, subtotal, cantidad) VALUES (?,?,?,?,?,?)");
            stmt2.setInt(1, id);
            stmt2.setInt(2, idP);
            stmt2.setString(3, "13343843031-1");
            stmt2.setString(4, "Pruebita");
            stmt2.setInt(5, 500);
            stmt2.setInt(6, 40);
            stmt2.execute();
            System.out.println("Insertados detalle");
        } catch (SQLException ex) {
            Logger.getLogger(JavaPostgreSQLBasic.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }
    
}   